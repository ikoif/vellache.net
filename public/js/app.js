var world = document.getElementById('world');
var $world = $('#world');

var filesArr = decode(FILES);
var extra = 1;			
var bonus = 2;
var rows = 4;
var cols = 4;

var placeholder = '../placeholder2.jpg';
var galleryPath = 'images/gallery/';

var backgroundBg = 'images/background.jpg';
var aboutBg = 'images/about.jpg'

var hoveredClass = 'hovered'
var hoveredExtraClass = 'extra'

var pieceWrapperClass = "piece-wrapper"
var pieceClass = "piece";
var piecePlaceholderClass = "placeholder"
var pieceExtraClass = "extra"
var pieceViewWrapperClass = "piece-view-wrapper";
var pieceViewFSWrapperClass = "piece-view-fs-wrapper";
var pieceViewClass = "piece-view";
var pieceViewBGClass = "piece-view-bg";
var pieceViewFSClass = "piece-view-fs";
var visibleClass = "active";

var tbl = document.createElement('table');
var tbdy = document.createElement('tbody');
var backgroundView = document.createElement('div');
var backgroundViewFS = document.createElement('div');

var cycle = 0;
var opts = {
    timeout: 300,
    interval: 0
};

// adjust TABLE styling and append TBODY to TABLE
tbl.appendChild(tbdy);

// add class to backgroundView div
backgroundView.classList.add(pieceViewWrapperClass);
backgroundViewFS.classList.add(pieceViewFSWrapperClass);

// add background logo to background view
var pieceViewBG = document.createElement('div');
pieceViewBG.classList.add(pieceViewBGClass);
pieceViewBG.style.backgroundImage = "url(images/background.jpg)";
// pieceView.id = 'pieceView-'+cycle;
backgroundView.appendChild(pieceViewBG);

// append the table to the world so we can work with it
world.appendChild(tbl)
world.appendChild(backgroundView)
world.appendChild(backgroundViewFS)

// add extra divs
for(var i = 0; i < extra; i++ ) {
    filesArr.splice(getRandomInt(0, filesArr.length), 0, 'extra');
}

// add bonus divs
for(var i = 0; i < bonus; i++ ) {
    filesArr.push('placeholder')
}



for (var i = 0; i < rows; i++) {

	// create TR and append to TBODY
    var tr = document.createElement('tr');
    tbdy.appendChild(tr);

    for (var j = 0; j < cols; j++) {

        var td = document.createElement('td');
        var pieceWrapper = document.createElement('div');
        var piece = document.createElement('div');

        pieceWrapper.classList.add(pieceWrapperClass);
        pieceWrapper.appendChild(piece);

        piece.classList.add(pieceClass);

        td.appendChild(pieceWrapper);
        td.style.width = (100 / cols) + '%';
  
        // append the TD to TR
        tr.appendChild(td);


        // is a piece
        var pieceView = document.createElement('div');
        var pieceViewFS = document.createElement('div');

        piece.style.backgroundImage = "url("+galleryPath+filesArr[cycle]+")";
        piece.setAttribute("data-index", cycle);

        pieceView.classList.add(pieceViewClass);
        pieceView.style.backgroundImage = "url("+galleryPath+filesArr[cycle]+")";
        pieceView.id = 'pieceView-'+cycle;

        pieceViewFS.classList.add(pieceViewFSClass);
        pieceViewFS.style.backgroundImage = "url("+galleryPath+filesArr[cycle]+")";
        pieceViewFS.id = 'pieceViewFS-'+cycle;

        // append the pieceView element to background view
        backgroundView.appendChild(pieceView);
        backgroundViewFS.appendChild(pieceViewFS);


        switch(filesArr[cycle]){
            case 'extra':
                console.log('extra')
                pieceWrapper.classList.add(pieceExtraClass);
                
                var bg = "url("+backgroundBg+")";

                piece.style.backgroundImage = bg;
                pieceView.style.backgroundImage = "url("+aboutBg+")";
                pieceViewFS.style.backgroundImage = "url("+aboutBg+")";
                
                hoverintent( piece ,
                function() {
                    // Handler in
                    showPieceView(this.getAttribute('data-index'), hoveredExtraClass);
                },
                function() {
                    // Handler out
                    hidePieceView(this.getAttribute('data-index'), hoveredExtraClass);
                }).options(opts);
                piece.onclick = function(){
                    showPieceViewFS(this.getAttribute('data-index'));
                }
                pieceViewFS.onclick = function(){
                    this.style.display = "none";
                    backgroundViewFS.style.display = "none";
                }
                break;
            case 'placeholder':
                pieceWrapper.classList.add(piecePlaceholderClass);
                
                piece.style.background = "none";
                
                hoverintent( piece ,
                function() {
                    // Handler in
                    showPieceView(this.getAttribute('data-index'));
                },
                function() {
                    // Handler out
                    hidePieceView(this.getAttribute('data-index'));
                }).options(opts);
                break;
            default:
                console.log('image')
    	        hoverintent( piece ,
    	        function() {
    	            // Handler in
    	            showPieceView(this.getAttribute('data-index'));
    	        },
    	        function() {
    	            // Handler out
    	            hidePieceView(this.getAttribute('data-index'));
    	        }).options(opts);

                piece.onclick = function(){
                    showPieceViewFS(this.getAttribute('data-index'));
                }
                pieceViewFS.onclick = function(){
                    this.style.display = "none";
                    backgroundViewFS.style.display = "none";
                }
                break;
        }


        cycle++;
    }
    // break;
}



// world.innerHTML += backgroundViewStart + backgroundViewBody + backgroundViewEnd;

console.log(filesArr, 'files with bonus and extra: '+ filesArr.length, 'result of rows:'+ rows +' and cols: '+ cols +' is :' + (rows * cols));

if(filesArr.length != rows*cols){
	console.error('not equal rows and cols', filesArr.length, rows * cols)
}

function getPieceView(index){
	return document.getElementById('pieceView-'+index)
}

var inContent = false;

function showPieceView(index, extraClass){
    if(index >= 0) getPieceView(index).classList.add(visibleClass);

    if(isAnythingVisible()){
        $world.addClass(hoveredClass);
    }else{
        $world.removeClass(hoveredClass);
    }

    if(extraClass) $world.addClass(extraClass)
}

function hidePieceView(index, extraClass){
    if(index >= 0) getPieceView(index).classList.remove(visibleClass);
    
    if(isAnythingVisible()){
        $world.addClass(hoveredClass);
    }else{
        $world.removeClass(hoveredClass);
    }

    if(extraClass) $world.removeClass(extraClass)
}


function showPieceViewFS(index){
    backgroundViewFS.style.display = "block";
    document.getElementById('pieceViewFS-'+index).style.display = "block";
}


function isAnythingVisible(){
	return $('.'+pieceViewWrapperClass+' .'+visibleClass).length > 0
}

function decode(encodedString) {
    var tmpElement = document.createElement('span');
    tmpElement.innerHTML = encodedString;
    return JSON.parse(tmpElement.innerHTML);
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
