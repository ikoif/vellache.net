var fs = require('fs');
var path = require('path');

module.exports = function(app) {
	var files = JSON.stringify(fs.readdirSync(path.join(__dirname,'../public/images/gallery/')));

    app.get('/', function(req, res) {
        res.render('../public/views/index', {
			FILES: files
		});
    });

    app.get('/remote', function(req, res) {
        res.render('../public/views/remote');
    });

    app.get('/experiment/:id', function(req, res) {
        res.render('../public/views/experiment_' + req.params.id);
    });
};