## A simple gallery portfolio

![site.png](https://bitbucket.org/repo/Go5b66/images/1297732283-site.png)

Based on the amount of images in the gallery folder, the content is looped. Values for amount of rows and columns have to be provided on the client side script and also provide how many to leave blank (in order to fit a square grid with any number of images, sometimes blank spaces have to be created)


## Live demo at [http://vellache.net](http://www.vellache.net)

## To run locally:

- clone the repo
- make sure you have nodejs installed (https://nodejs.org/en/)
- $ npm install
- $ npm run start
- should be running on http://localhost:8080
- enjoy :)